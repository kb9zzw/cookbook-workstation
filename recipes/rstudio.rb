#
# Cookbook:: workstation
# Recipe:: rstudio
#
# Copyright:: 2019, The Authors, All Rights Reserved.
#

# This recipe installs RStudio Desktop

case node[:platform]

# RPM install for rhel-flavored o/s
when 'rhel', 'centos', 'amazon'
  remote_file "#{Chef::Config[:file_cache_path]}/rstudio-1.2.1335-x86_64.rpm" do
    source 'https://download1.rstudio.org/desktop/centos7/x86_64/rstudio-1.2.1335-x86_64.rpm'
    action :create
  end
  rpm_package 'rstudio' do
    source "#{Chef::Config[:file_cache_path]}/rstudio-1.2.1335-x86_64.rpm"
    action :install
  end

# DEB install for ubuntu 18+
when 'ubuntu'
  remote_file "#{Chef::Config[:file_cache_path]}/rstudio-1.2.1335-amd64.deb" do
    source 'https://download1.rstudio.org/desktop/bionic/amd64/rstudio-1.2.1335-amd64.deb'
    action :create
  end
  dpkg_package 'rstudio' do
    source "#{Chef::Config[:file_cache_path]}/rstudio-1.2.1335-amd64.deb"
    action :install
  end

# DEB install for debian 9+
when 'debian'
  remote_file "#{Chef::Config[:file_cache_path]}/rstudio-1.2.1335-amd64.deb" do
    source 'https://download1.rstudio.org/desktop/debian9/x86_64/rstudio-1.2.1335-amd64.deb'
    action :create
  end
  dpkg_package 'rstudio' do
    source "#{Chef::Config[:file_cache_path]}/rstudio-1.2.1335-amd64.deb"
    action :install
  end
end
