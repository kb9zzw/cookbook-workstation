#
# Cookbook:: workstation
# Recipe:: vscode
#
# Copyright:: 2019, The Authors, All Rights Reserved.
#

# This recipe installs Microsoft Visual Sudio Code

repo_options = {
  :minrate => '10'
}

# install microsoft-vscode repos
case node[:platform_family]
when 'debian'
  apt_repository 'microsoft-vscode' do
    uri 'https://packages.microsoft.com/repos/vscode'
    key 'https://packages.microsoft.com/keys/microsoft.asc'
    distribution 'stable'
    components ['main']
  end
when 'rhel', 'amazon'
  yum_repository 'microsoft-vscode' do
    baseurl 'https://packages.microsoft.com/yumrepos/vscode'
    gpgkey 'https://packages.microsoft.com/keys/microsoft.asc'
    timeout '60'
    options repo_options
  end
end

# install vscode
package 'code' do
  action :install
end
