#
# Cookbook:: workstation
# Recipe:: cloudfoundry
#
# Copyright:: 2019, The Authors, All Rights Reserved.
#

# This recipe install the CloudFoundry CLI

repo_options = {
  :minrate => '10'
}

# install cloudfoundry repos
case node[:platform_family]
when 'debian'
  apt_repository 'cloudfoundry-cli' do
    uri 'https://packages.cloudfoundry.org/debian'
    key 'https://packages.cloudfoundry.org/debian/cli.cloudfoundry.org.key'
    distribution 'stable'
    components ['main']
  end
when 'rhel', 'amazon'
  yum_repository 'cloudfoundry-cli' do
    baseurl 'https://packages.cloudfoundry.org/fedora'
    gpgkey 'https://packages.cloudfoundry.org/fedora/cli.cloudfoundry.org.key'
    timeout '60'
    options repo_options
  end
end

# install packages
package 'cf-cli' do
  action :install
end
