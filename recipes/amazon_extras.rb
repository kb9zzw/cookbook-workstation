#
# Cookbook:: workstation
# Recipe:: amazon_extras
#
# Copyright:: 2019, The Authors, All Rights Reserved.
#

# This recipe installs extras packages provided by amazon-linux-extras

case node[:platform]
when 'amazon'
  node[:cookbook_name][:amazon_extras].each do |pkg|
    execute 'amazon-linux-extras' do
      command "/usr/bin/amazon-linux-extras install -y #{pkg}"
    end
  end
end
