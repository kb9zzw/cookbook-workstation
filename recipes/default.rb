#
# Cookbook:: workstation
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.
#
include_recipe '::base_packages'
include_recipe '::amazon_extras'
include_recipe '::chrome'
include_recipe '::chefdk'
include_recipe '::atom'
include_recipe '::vscode'
include_recipe '::cf-cli'
include_recipe '::rstudio'
include_recipe '::anaconda'
