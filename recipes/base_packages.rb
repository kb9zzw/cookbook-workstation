#
# Cookbook:: workstation
# Recipe:: base_packages
#
# Copyright:: 2019, The Authors, All Rights Reserved.
#

# This recipe is meant to install standard system packages that do not require special treatment

case node[:platform]

# Setup epel for centos
when 'centos'
  package 'epel-release' do
    action :install
 end

# Setup epel on rhel
when 'rhel'
  remote_file "#{Chef::Config[:file_cache_path]}/epel-release-latest-7.noarch.rpm" do
    source 'https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm'
    action :create
  end
  rpm_package 'epel-release-latest-7' do
    source "#{Chef::Config[:file_cache_path]}/epel-release-latest-7.noarch.rpm"
    action :install
  end

# Setup amazon-linux-extras and epel for amazon linux
when 'amazon'
  package 'amazon-linux-extras' do
    action :install
  end
  execute 'amazon-linux-extras' do
    command '/usr/bin/amazon-linux-extras install -y epel'
    creates '/etc/yum.repos..d/epel.repo'
  end
end


# install base_packages
node[:cookbook_name][:packages].each do |pkg|
  package pkg do
    action :install
  end
end
