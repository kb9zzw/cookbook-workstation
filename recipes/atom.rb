#
# Cookbook:: workstation
# Recipe:: atom
#
# Copyright:: 2019, The Authors, All Rights Reserved.
#

# This recipe installs Atom

repo_options = {
  :minrate => '10'
}

# install atom repository
case node[:platform_family]
when 'debian'
  apt_repository 'atom' do
    uri 'https://packagecloud.io/AtomEditor/atom/any/'
    key 'https://packagecloud.io/AtomEditor/atom/gpgkey'
    distribution 'any'
    components ['main']
  end
when 'rhel', 'amazon'
  yum_repository 'atom' do
    baseurl 'https://packagecloud.io/AtomEditor/atom/el/7/\$basearch'
    gpgkey 'https://packagecloud.io/AtomEditor/atom/gpgkey'
    gpgcheck false
    timeout '60'
    options repo_options
  end
end

# install atom
package 'atom' do
  action :install
end
