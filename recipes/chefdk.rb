#
# Cookbook:: workstation
# Recipe:: chefdk
#
# Copyright:: 2019, The Authors, All Rights Reserved.
#

# This recipe installs ChefDK

case node[:platform]

# RPM install for rhel-flavored o/s
when 'rhel', 'centos', 'amazon'
  remote_file "#{Chef::Config[:file_cache_path]}/chefdk-4.0.60-1.el7.x86_64.rpm" do
    source 'https://packages.chef.io/files/stable/chefdk/4.0.60/el/7/chefdk-4.0.60-1.el7.x86_64.rpm'
    action :create
  end
  rpm_package 'chefdk' do
    source "#{Chef::Config[:file_cache_path]}/chefdk-4.0.60-1.el7.x86_64.rpm"
    action :install
  end

# DEB install for ubuntu 18+
when 'ubuntu'
  remote_file "#{Chef::Config[:file_cache_path]}/chefdk_4.0.60-1_amd64.deb" do
    source 'https://packages.chef.io/files/stable/chefdk/4.0.60/ubuntu/18.04/chefdk_4.0.60-1_amd64.deb'
    action :create
  end
  dpkg_package 'chefdk' do
    source "#{Chef::Config[:file_cache_path]}/chefdk_4.0.60-1_amd64.deb"
    action :install
  end

# DEB install for debian 9+
when 'debian'
  remote_file "#{Chef::Config[:file_cache_path]}/chefdk_4.0.60-1_amd64.deb" do
    source 'https://packages.chef.io/files/stable/chefdk/4.0.60/debian/9/chefdk_4.0.60-1_amd64.deb'
    action :create
  end
  dpkg_package 'rstudio' do
    source "#{Chef::Config[:file_cache_path]}/chefdk_4.0.60-1_amd64.deb"
    action :install
  end
end
