#
# Cookbook:: workstation
# Recipe:: chrome
#
# Copyright:: 2019, The Authors, All Rights Reserved.
#

# This recipe installs Google Chrome

repo_options = {
  :minrate => '10'
}

# install google-chrome repos
case node[:platform_family]
when 'debian'
  apt_repository 'google-chrome' do
    uri 'http://dl.google.com/linux/chrome/deb/'
    key 'https://dl.google.com/linux/linux_signing_key.pub'
    distribution 'stable'
    components ['main']
  end
when 'rhel', 'amazon'
  yum_repository 'google-chrome' do
    baseurl 'http://dl.google.com/linux/chrome/rpm/stable/$basearch'
    gpgkey 'https://dl-ssl.google.com/linux/linux_signing_key.pub'
    timeout '60'
    options repo_options
  end
end

# install google-chrome
package 'google-chrome-stable' do
  action :install
end
