#
# Cookbook:: workstation
# Recipe:: anaconda
#
# Copyright:: 2019, The Authors, All Rights Reserved.
#

# This recipe installs Anaconda

remote_file "#{Chef::Config[:file_cache_path]}/Anaconda3-2019.03-Linux-x86_64.sh" do
  source 'https://repo.anaconda.com/archive/Anaconda3-2019.03-Linux-x86_64.sh'
  mode '0755'
  action :create
end
execute 'Anaconda3' do
  command "bash -c \'#{Chef::Config[:file_cache_path]}/Anaconda3-2019.03-Linux-x86_64.sh -b -p /opt/anaconda\'"
  creates '/opt/anaconda'
end
