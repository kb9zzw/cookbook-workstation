case node[:platform_family]
when 'debian'
  default[:cookbook_name][:packages] = [
    'git',
    'terminator',
    'docker',
    'python3',
    'gimp',
    'r-base',
    'r-base-dev',
    'pgadmin3',
    'libreoffice-writer',
    'libreoffice-calc',
    'libreoffice-draw',
    'libreoffice-impress',
    'yamllint',
    'pylint',
    'neovim',
    'firefox',
    'mlocate',
    'curl',
    'wget',
    'bzip2',
    'htop',
    'dnsutils',
    'openjdk-8-jre',
    'openjdk-8-jdk',
    'libclang-dev',
  ]

when 'rhel'
  default[:cookbook_name][:packages] = [
    'git',
    'terminator',
    'docker',
    'python36',
    'gimp',
    'R',
    'R-devel',
    'pgadmin3',
    'libreoffice-writer',
    'libreoffice-calc',
    'libreoffice-draw',
    'libreoffice-impress',
    'yamllint',
    'pylint',
    'neovim',
    'firefox',
    'mlocate',
    'curl',
    'wget',
    'bzip2',
    'bind-utils',
    'htop',
    'nodejs',
    'java-1.8.0-openjdk',
    'java-1.8.0-openjdk-devel',
  ]

when 'amazon'
  default[:cookbook_name][:packages] = [
    'git',
    'terminator',
    'python36',
    'R',
    'R-devel',
    'pgadmin3',
    'yamllint',
    'pylint',
    'neovim',
    'mlocate',
    'curl',
    'wget',
    'bind-utils',
    'bzip2',
    'nodejs',
    'java-1.8.0-openjdk',
    'java-1.8.0-openjdk-devel',
  ]

  default[:cookbook_name][:amazon_extras] = [
    'gimp',
    'docker',
    'libreoffice'
  ]
end
