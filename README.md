# Workstation Bootstrap Cookbook

## Enough jabber, how do I install it?

Run the following as root:

```
curl -sL https://gitlab.com/kb9zzw/cookbook-workstation/raw/master/bootstrap.sh | bash
```

Alternatively, clone this repository and run the "bootstrap" script as root:

```
sudo ./bootstrap.sh
```

## So, what is this?

This is a Chef cookbook that is designed to bootstrap a basic Linux system with
common software used for development and data analysis.  It is designed to run in stand-alone
mode without access to a central Chef server using publicly accessible software repositories and packages.

At the moment, three "flavors" of Linux are supported:
* Ubuntu 18.04
* CentOS7
* Amazon Linux 2

Other Linux flavors such as Debian 9+ and RedHat Enterprise Linux 7 may also work, but are untested.

## Wonderful, but what does it do?

This cookbook installs a pre-defined set of software packages from operating system-provided package repositories.  Additionally, a few special recipes are included to install software that isn't typically available from the O/S vendors.

The general preference is as follows:
- Install from O/S-native standard repositories (yum or apt)
- Install from special or 3rd-party repositories (epel, ppa, or others; via yum or apt)
- Install from third-party packages (RPM or dpkg)
- Install using a vendor-provided install script

In general, this cookbook prefers repository-provided packages to potentially newer third-party packages, even if the third-party packages are newer.

## What does it install?

This is under development, so the exact package list may change from time to time.  Here's what it currently provides:

* Common utilities (curl, wget, htop, neovim, terminator, etc.)
* Git client
* Language development kits (Java JDK, NodeJS, Python, R)
* Integrated development environments (VS Code, Atom, RStudio)
* PaaS/Cloud/CM tools (Cloud Foundry CLI, Docker)
* Data Science tools (Anaconda)
* Database tools (postgresql, pgadmin)
* Office productivity tools (LibreOffice, GIMP)
* Web browsers (Firefox, Chrome)
* Handful of other useful tidbits

## But I don't like this software list!

'tis fine.  Customize to suit.  Couple of ways to do this:

### For base packages

Modify the ```attributes/default.rb``` list for your operating system and add/remove packages.  Note that Amazon Linux also has a separate list for "amazon-linunx-extras", independent from the package list.

### For custom stuff

More complex installs are handled by their own recipes under recipes/*.rb.(i.e. anaconda.rb).  You can
ommit these by commenting out the include statement for the recipe in ```default.rb```.

You can also fork this repo and develop your own recipes if you have something special.

## What if I want to make changes after I run it once?

The bootstrap script downloads itself to /root/.chef/cookbooks/workstation from Gitlab.  You can locally edit it, then run it again using the following commands:

```
# Become root and visit the cookbook project folder
sudo -i
cd /root/.chef

# Suck down the latest version from Gitlab, if desired
git pull origin master

# Run the cookbook
chef-client -z -o workstation
```

## Any other thoughts?

Yes, this cookbook doesn't CONFIGURE the software packages for your use, it simply installs them.  Everyone's tastes are different, so you'll have to configure these packages to meet your needs.  Things you may have to do:

* Install other packages not in this list
* Setup your IDE
* Install modules or libraries (R, Python, conda, etc.)
* and bunches of other things

Hopefully you find this useful and gets you started faster.

Now, go build something great!
