#

# Bootstrap script

# Install chef-client
if [ ! -e "/opt/chef/bin/chef-client" ]; then
  export CHEF_LICENSE=accept
  curl -L https://omnitruck.chef.io/install.sh | bash
fi

# Retrieve latest workstation cookbook
if [ ! -e "~/.chef/cookbooks/workstation" ]; then
  mkdir -p ~/.chef/cookbooks
  git clone https://gitlab.com/kb9zzw/cookbook-workstation.git ~/.chef/cookbooks/workstation
else
  pushd ~/.chef/cookbooks/workstation
  git pull origin master
  popd
fi

# Run the coookbook
pushd ~/.chef
chef-client -z -o workstation
popd
